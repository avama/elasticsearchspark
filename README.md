# Elasticsearchspark POC
 This is a sample poc project practicing reading data from/writing data to Elasticsearch.
 It also practices some basic spark core and ml features with spark inbuilt `LogisticRegression` model, 
 trains the test data and detect the scam email message.

# Local Development
### Starting the Application
* Run `mvn clean package`
* Run `docker-compose up`
* Local Spark master UI is available at `http://localhost:8080`
* Local Elasticsearch UI is available at `http://localhost:5000/#!/`, 
  use the default connection url `http://elastic-search:9200` to connect to local Elasticsearch server
* Run the sample app
  ssh to spark-master container: `docker exec -it spark-master bash`
  submit the spark job: `./spark/bin/spark-submit --class com.nextiva.elasticsearchspark.ElasticsearchSparkMlPoc --master local[*] /spark/jars/app/elasticsearch-spark-1.0.0-SNAPSHOT.jar /spark/data/fraud_email_.csv `
