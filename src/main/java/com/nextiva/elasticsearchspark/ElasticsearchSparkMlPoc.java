/*
 * Copyright (c) 2021 Nextiva, Inc. to Present.
 * All rights reserved.
 */

package com.nextiva.elasticsearchspark;

import java.util.Arrays;

import org.apache.commons.math3.util.Precision;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.classification.LogisticRegressionModel;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.feature.IDF;
import org.apache.spark.ml.feature.IDFModel;
import org.apache.spark.ml.feature.StopWordsRemover;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.size;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.split;

/**
 * Poc class to practice basic funcs of spark core/ml/es
 */
public class ElasticsearchSparkMlPoc {

    public static void main(String[] args) {

        if (args.length < 1) {
            System.err.println("Missing argument for input file.");
            System.exit(1);
        }

        String dataPath = args[0];
        SparkEsUtils sparkEsUtils = new SparkEsUtils();
        String index = "testemail/doc";

        Dataset<Row> dataset = sparkEsUtils.readFromCsv(dataPath);

         //Write data to elasticsearch
        sparkEsUtils.writeToEs(dataset, index);

        // Read the index data from elasticsearch
        Dataset<Row> esDataset = sparkEsUtils.readDfFromEs(index);
        System.out.println("Successfully load data from elasticsearch.");

        long dataCount = esDataset.count();
        String sampleData = esDataset.showString(3, 0, true);
        System.out.println("Data total count is : " + dataCount);
        System.out.println("Sample data would looks like: " + sampleData);
        esDataset = esDataset.selectExpr("Class as label", "Text");

        esDataset = esDataset.withColumn("label", esDataset.col("label").cast(DataTypes.IntegerType));

        System.out.println("Data schema : " + dataCount);
        esDataset.printSchema();

        // Pick only the first 300 rows of dataset considering the cpu/memory capacity.
        // Increase the number of dataset would increase result accuracy.
        esDataset = sparkEsUtils.createDataSet(Arrays.asList((Row[])(esDataset.head(300))), esDataset.schema());

        // Removing missing data to lower the bias.
        String[] dropCol = {"label"};
        esDataset = esDataset.na().drop(dropCol);
        System.out.println("Data total count after removing missing data : "
                + esDataset.filter(esDataset.col("label").isNotNull()).count());

        System.out.println("Group by label result: ");
        esDataset.groupBy("label").count().orderBy("count").show();

        esDataset = esDataset.withColumn("words", split(col("Text")," "));
        esDataset = esDataset.withColumn("word_count", size(split(col("Text")," ")));
        System.out.println("Text word count result: ");
        esDataset.show(10);

        esDataset.filter(esDataset.col("label").isin("0", "1")).collect();
        esDataset.show(10);

        // Remove common words to improve performance and accuracy.
        StopWordsRemover stopwordsRemovalFeature = new StopWordsRemover();
        stopwordsRemovalFeature.setInputCol("words");
        stopwordsRemovalFeature.setOutputCol("words without stop");
        esDataset = stopwordsRemovalFeature.transform(esDataset);

        // Extract features by TF-IDF for data training.
        HashingTF hashingTF =
                new HashingTF().setInputCol("words without stop").setOutputCol("rawFeatures").setNumFeatures(500);
        esDataset = hashingTF.transform(esDataset);

        esDataset.show(10);

        IDF idf = new IDF();
        idf.setInputCol("rawFeatures");
        idf.setOutputCol("features");
        IDFModel idfModel = idf.fit(esDataset);
        esDataset = idfModel.transform(esDataset);

        esDataset.show(10);

        double [] weights = new double[]{0.75, 0.25};
        Dataset<Row>[] datasets = esDataset.randomSplit(weights, 7L);
        Dataset<Row> trainingDF = datasets[0];
        Dataset<Row> testDF = datasets[1];

        LogisticRegression logreg = new LogisticRegression().setRegParam(0.25);

        LogisticRegressionModel logregModel = logreg.fit(trainingDF);
        // Save the LogisticRegression Model
        // logregModel.save("scam_email_detection.model");
        Dataset<Row> predictionDF = logregModel.transform(testDF);

        System.out.println("Expected result: ");
        predictionDF.select("label", "probability", "prediction").show(20);

        MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
                .setMetricName("accuracy");
        double accuracyScore = evaluator.evaluate(predictionDF.select("prediction", "label"));
        System.out.println("Accuracy score: " + Precision.round(accuracyScore, 3) * 100);

        sparkEsUtils.stop();
    }
}
