/*
 * Copyright (c) 2021 Nextiva, Inc. to Present.
 * All rights reserved.
 */

package com.nextiva.elasticsearchspark;

import java.util.List;
import java.util.Map;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.input.PortableDataStream;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;

/**
 * Util class to handle some basic spark core operations.
 */
public final class SparkEsUtils {

    private final SparkSession sparkSession = initializeSession();
    private final SparkContext sparkContext = sparkSession.sparkContext();
    private final JavaSparkContext javaSparkContext = JavaSparkContext.fromSparkContext(sparkContext);

    public Dataset<Row> createDataSet(List<Row> rows, StructType schema) {
        return sparkSession.createDataFrame(rows, schema);
    }

    public void writeTextFileToEs(String inputPath, String index) {
        JavaRDD<String> javaRDD = javaSparkContext.textFile(inputPath);
        JavaEsSpark.saveToEs(javaRDD, index);
    }

    public void writeListToEs(List<?> input, String index) {
        JavaRDD<?> javaRDD = javaSparkContext.parallelize(input);
        JavaEsSpark.saveToEs(javaRDD, index);
    }

    public void writeObjectFileToEs(String inputPath, String index) {
        JavaRDD<String> javaRDD = javaSparkContext.objectFile(inputPath);
        JavaEsSpark.saveToEs(javaRDD, index);
    }

    public void writeBinaryFileToEs(String inputPath, String index) {
        JavaPairRDD<String, PortableDataStream> javaRDD = javaSparkContext.binaryFiles(inputPath);
        JavaEsSpark.saveToEsWithMeta(javaRDD, index);
    }

    public void writeJsonToEs(JavaRDD<String> input, String index) {
        JavaEsSpark.saveJsonToEs(input, index);
    }

    public void writeToEs(JavaRDD<?> input, String index) {
        JavaEsSpark.saveToEs(input, index);
    }

    public void writeToEs(Dataset<?> input, String index) {
        JavaEsSparkSQL.saveToEs(input, index);
    }

    public Dataset<Row> readFromCsv(String inputPath) {
        return sparkSession.read().format("csv")
                .option("header", "true")
                .load(inputPath);
    }

    public Dataset<Row> readDfFromEs(String index) {
        return JavaEsSparkSQL.esDF(sparkSession, index);
    }

    public JavaPairRDD<String, Map<String, Object>> readFromEsWithQuery(String index, String query) {
        return JavaEsSpark.esRDD(javaSparkContext, index, query);
    }

    public JavaPairRDD<String, Map<String, Object>> readFromEs(String index) {
        return JavaEsSpark.esRDD(javaSparkContext, index);
    }

    public JavaPairRDD<String, String> readJsonFromEs(String index) {
        return JavaEsSpark.esJsonRDD(javaSparkContext, index);
    }

    public JavaPairRDD<String, String> readJsonFromEsWithQuery(String index, String query) {
        return JavaEsSpark.esJsonRDD(javaSparkContext, index, query);
    }

    public void stop() {
        sparkSession.stop();
    }

    private SparkSession initializeSession() {
        return SparkSession
                .builder()
                .appName("elasticsearch-spark")
                .master("local[*]")
                .config("spark.es.nodes","elasticsearch")
                .config("spark.es.port","9200")
                .config("spark.es.net.http.auth.user", "elastic")
                .config("spark.es.net.http.auth.pass", "changeme")
                .getOrCreate();
    }
}
